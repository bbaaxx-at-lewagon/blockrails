# This will go to the docker repos and will look for an image
# identified by the string: "ruby:2.7"
FROM ruby:2.7

# Update container and install some stuff
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client

# Create a dir on the container, copy files and run bundle
RUN mkdir /myapp
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install

# We copy the rest of the files last
COPY . /myap

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

# Expose the port for the rails app runing in the container
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]

