## Create a new app:

```
docker-compose run web rails new . --force --no-deps --database=postgresql
docker-compose build
```

Once the application is up and running, create the database with:

````
docker-compose run web rake db:create
````
